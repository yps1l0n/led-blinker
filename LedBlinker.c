#ifndef F_CPU
    #define F_CPU               16000000
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

// number, position and port of (first) connected LEDs
#define LEDPORT                 PORTD
#define LEDDDR                  DDRD
#define LED0                    2
#define LEDNUMBER               3

// definitions regarding USART
#define USART_UBRR0value        103

// flag shows if usart completed receiving the string
volatile uint8_t usart_rx_string_complete = 0;

// received string - valid while usart_rx_string_complete is set
volatile uint8_t usart_rx_string[LEDNUMBER * 2] = "";

// half of the new period length of the led frequencies
volatile uint16_t half_period_length_new[LEDNUMBER];

// flag to indicate a new synchronisation
volatile uint8_t sync_trigger = 0;

/// Interrupts every 1 ms to toggle the appropriate leds and update the period lengths.
///
ISR(TIMER0_COMPA_vect){
    static uint32_t uptime = 0;
    static uint32_t half_period_length[LEDNUMBER];
    static uint32_t next_toggle_time[LEDNUMBER];
    uint8_t led_port_tmp;

    ++uptime;
    // TODO handle overflow of the uptime (approximately 50d) and related overflow of next_toggle_times
    led_port_tmp = 0;

    // for syncing restart uptime and currently blinking LEDs
    if(sync_trigger){
        uptime = 0;
        for(uint8_t i=0; i<LEDNUMBER; ++i){
            if(next_toggle_time[i] < 0xFFFFFFFF){
                next_toggle_time[i] = half_period_length[i];
            }
        }
        LEDPORT &= ~((0xFF>>(8-LEDNUMBER))<<LED0);
        sync_trigger = 0;
    }

    for(uint8_t i=0; i<LEDNUMBER; ++i){
        // update the half_period_length* and corresponding next_toggle_time*
        // half_period_length_new[*] = 0 does nothing
        // half_period_length_new[*] = 1 switches the LED off
        // any other value sets new frequency
        if(half_period_length_new[i]){
            if(half_period_length_new[i] == 1){
                next_toggle_time[i] = 0xFFFFFFFF;
                LEDPORT &= ~(1<<(LED0 + i));
            }else if(next_toggle_time[i] == 0xFFFFFFFF){
                next_toggle_time[i] = uptime;
            }else{
                // If the frequency changes, the next toggle will be aligned with the last toggle of the previous frequency to avoid single shorter cycles. (Use sync_trigger feature to align led blinking if wanted. )
                next_toggle_time[i] = next_toggle_time[i] - half_period_length[i] + half_period_length_new[i];
            }
            half_period_length[i] = half_period_length_new[i];
            half_period_length_new[i] = 0;
        }

        // toggle the appropriate leds and update next_toggle_time*
        if(uptime >= next_toggle_time[i]){
            next_toggle_time[i] += half_period_length[i];
            led_port_tmp |= 1<<(LED0 + i);
        }
    }

    LEDPORT ^= led_port_tmp;
}

/// Sets the led pins as outputs.
///
void LedInit(void){
    LEDDDR |= (0xFF>>(8-LEDNUMBER))<<LED0;

    for(uint8_t i=0; i<LEDNUMBER; ++i){
        half_period_length_new[i] = 1;
    }
}

/// Ititializes the timer 0.
///
void TimerInit(void){
    // CTC mode
    TCCR0B &= ~(1<<WGM02);
    TCCR0A |= 1<<WGM02;
    TCCR0A &= ~(1<<WGM02);

    // prescale 1/64
    TCCR0B &= ~(1<<CS02);
    TCCR0B |= 1<<CS01;
    TCCR0B |= 1<<CS00;

    // set CTC limit to 0d250-1 (1ms @ 16MHz, 1/64)
    OCR0A = 0xF9;

    // set compare interrupt
    TIMSK0 |= 1<<OCIE0A;

    sei();
}

/// Initializes USART.
///
void UsartInit(void){
    // set baudrate
    UBRR0H = USART_UBRR0value >> 8;
    UBRR0L = USART_UBRR0value;

    // enable rx complete interrupt, rx and tx
    UCSR0B = (1<<RXCIE0) | (1<<RXEN0) | (1<<TXEN0);

    // set character size to 8 bit
    UCSR0C = (1<<UCSZ00) | (1<<UCSZ01);

    sei();
}

/// Transmits a single character via USART.
///
void UsartTransmitChar(uint8_t c){
    while(!(UCSR0A & (1<<UDRE0)));
    UDR0 = c;
}

/// Transmits a 0x00 terminated string via USART.
///
void UsartTransmitString(uint8_t *ca){
    while(*ca){
        UsartTransmitChar(*ca);
        ++ca;
    }
    UsartTransmitChar(0x00);
}

/// Interrupts if USART received a character and add it to usart_rx_string.
///
ISR(USART_RX_vect){
    static uint8_t usart_rx_string_len = 0;
    uint8_t new_rx_char = UDR0;

    if(!usart_rx_string_complete){
        usart_rx_string[usart_rx_string_len++] = new_rx_char;
        if(usart_rx_string_len >= LEDNUMBER * 2){
            usart_rx_string_len = 0;
            usart_rx_string_complete = 1;
        }
    }
    // TODO handle erroneous messages
}

/// Resets the usart_rx_string_complete flag.
///
void ResetUsartCompleteFlag(){
    usart_rx_string_complete = 0;
}

/// Flashes the onboard Debug-LED.
///
void DebugFlash(){
    DDRB |= 1<<PB5;
    PORTB |= 1<<PB5;
    _delay_ms(100);
    PORTB &= ~(1<<PB5);
}

/// Sets new period lengths from USART input. Returns 1 if new data was available, 0 elsewise.
///
uint8_t SetNewPeriodLengths(void){
    if(usart_rx_string_complete){
        uint8_t check_sync_command = 0;
        for(uint8_t i=0; i<LEDNUMBER; ++i){
            half_period_length_new[i] = ((uint16_t)(usart_rx_string[2*i]) << 8) | usart_rx_string[2*i+1];
            check_sync_command |= usart_rx_string[2*i] | usart_rx_string[2*i+1];
        }

        // if every received byte is 0x00, syncronise all LEDs
        if(!check_sync_command){
            sync_trigger = 1;
        }
        ResetUsartCompleteFlag();
        DebugFlash();
        return 1;
    }else{
        return 0;
    }
}

/// Main.
///
int main(void){
    LedInit();
    TimerInit();
    UsartInit();

    UsartTransmitString("I'm awake :)");
    DebugFlash();

    while(1){
        SetNewPeriodLengths();
    }
}
