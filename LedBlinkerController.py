#!/usr/bin/python3

import sys
import re
from time import sleep
from os.path import isfile

import numpy as np
import serial

NUMBER_OF_LEDS = 3


def init_serial_connection(serial_port='/dev/ttyUSB0', wait_after_reset=True):
    """Initialize a serial connection for USART communication with an uC."""

    serial_connection = serial.Serial()
    serial_connection.timeout = 1
    serial_connection.baudrate = 9600
    serial_connection.setPort(serial_port)
    serial_connection.open()

    # Depending how the serial connection will be established the uC may be
    # resetted. E.g. If the connection is made from an standalone Python3
    # script to an arduino. In this case wait for some time to prevent losing
    # data packets.
    if wait_after_reset:
        sleep(2)

    return serial_connection


def transmit_sync_command(serial_connection):
    """Transmit the command for synchronising the LEDs."""

    # sync_command is just 0x0000 times number of LED
    # (indicating that no frequency should change)
    # synchronisation will change the phase of the blinking,
    # deactivate it if this behaviour is unacceptable
    serial_connection.write(bytes(NUMBER_OF_LEDS*2))


def transmit_data(led_data_set, serial_connection, sync=True):
    """Transmit data via the given serial connection."""

    # data_command consists of 2 bytes per LED:
    # 0x0000 to ignore the LED,
    # 0x0001 to switch off the LED,
    # any other value to set half_period_length of LED in ms
    serial_connection.write(led_data_set.byteswap().tobytes())
    if sync:
        transmit_sync_command(serial_connection)


def frequency_to_half_period_length(frequency):
    """Convert a frequency to half of its period length or '1' for 'off'."""

    if frequency == 0:
        return 1
    else:
        return 1000/frequency/2


def parse_led_script_line(line):
    """Parse a single line of the appropriate script."""

    if re.match(r'^\s*[0-9]+\s+[0-9]+\s+[0-9]+(\.[0-9]*)?', line) is None:
        raise ValueError("Invalid format of line '" + line.rstrip() + "'")
    splitted_line = line.rstrip().split()

    delay = int(splitted_line[0])
    index = int(splitted_line[1])
    frequency = float(splitted_line[2])

    if index > NUMBER_OF_LEDS:
        raise ValueError(
                "LED index out of bounds (in line '"
                + line.rstrip() + "')")
    if frequency > 250:
        raise ValueError(
                "Frequency out of bounds (in line '"
                + line.rstrip() + "')")
    if frequency < 1000/0xFFFF/2 and frequency != 0:
        raise ValueError(
                "Frequency out of bounds (in line '"
                + line.rstrip() + "')")

    return delay, index, frequency_to_half_period_length(frequency)


def process_script(path_to_script, serial_connection):
    """Process the instructions of an appropriate script."""

    led_data_set = np.zeros(NUMBER_OF_LEDS, dtype=np.uint16)
    for line in open(path_to_script):
        delay, index, data = parse_led_script_line(line)
        if delay > 0:
            if not (led_data_set == 0).all():
                transmit_data(led_data_set, serial_connection)
                led_data_set *= 0
            print('waiting for ' + str(delay).rjust(5) + ' ms...')
            sleep(delay/1000)
        led_data_set[index] = data
    transmit_data(led_data_set, serial_connection)


def print_interactive_help():
    """Print the help text of the interactive mode."""

    print(
            "Please enter\n"
            + "  ╠ index of LED followed by an frequency\n"
            + "  ║      (e.g. [0 0.5] )\n"
            + "  ╠ path to an appropriate led script\n"
            + "  ║      (e.g. [LedBlinkerScript.txt]\n"
            + "  ╠ [s] for syncing the LEDs\n"
            + "  ╠ [q] to quit the program\n"
            + "  ╚ [h] show this help\n")


def interactive_mode(serial_connection):
    """Control the LEDs interactively."""

    print_interactive_help()

    while(True):
        interactive_input = input("═══ ")

        if interactive_input == "q":
            break
        elif interactive_input == "s":
            transmit_sync_command(serial_connection)
        elif interactive_input == "h":
            print_interactive_help()
        elif re.fullmatch(
                r'^[0-9]+\s[0-9]+(\.[0-9]+)?$',
                interactive_input
                ) is not None:
            delay, index, data = parse_led_script_line('0 '+interactive_input)
            led_data_set = np.zeros(NUMBER_OF_LEDS, dtype=np.uint16)
            led_data_set[index] = data
            transmit_data(led_data_set, serial_connection, sync=False)
        elif isfile(interactive_input):
            process_script(interactive_input, serial_connection)
            print()
            print_interactive_help()
        else:
            print("invalid input\n")


def main():
    """Main."""

    serial_connection = init_serial_connection()

    if len(sys.argv) > 1:
        if isfile(sys.argv[1]):
            process_script(sys.argv[1], serial_connection)
        else:
            print("invalid path")
    else:
        try:
            interactive_mode(serial_connection)
        except KeyboardInterrupt:
            pass

    serial_connection.close()


if __name__ == "__main__":
    main()
