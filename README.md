# LED-Blinker

A small project to control an easy blinking device. It consists of a LED attachment e.g. for an Arduino Nano, the C code for the Arduino and a Python3 script for controlling this via USART. It is used to have blinking LEDs with individually configurable frequencies.

## Usage

The Python3 script `LedBlinkerController.py` is meant to be used with an additional script file. This file defines a sequence of frequency changing commands. Therefore each line of the file must consist of three values:

    delay led_index frequency

Where *delay* is the time in milliseconds to delay from the last command. To affect multiple LEDs at the same time, there can be successive commands with a delay of *0*.

*led_index* is the index of the LED, which should be controlled.

And *frequency* is the choosen blinking frequency. It can be selcted between *250* Hz and ~*0.0077* Hz (~*1/130* s). To turn a LED *off*, a frequency of *0* is used.

---

`LedBlinkerScript.txt` is an example of such a sequence. It can be used by typing

    python3 LedBlinkerController.py LedBlinkerScript.txt

into the command line after changing to the appropriate directory by `cd path_to_directory/LedBlinker`. Obviously there must be a connected microcontroller with the given firmware. The sourcecode of the firmware for the Arduino can be found in `LedBlinker.c`.

Alternatively to the batch mode the Python3 script can be used in an interactive mode with an absolute minimal UI by running

    python3 LedBlinkerController.py

In this mode it is possible to load the led script file as well. Additionally a new frequency for a LED can be inputted directly or it can be deactivated by setting its frequency to `0`. If the frequency of a LED changes, the new blinking cycle will start at the time of the previous toggle to get smoother transitions. In the interactive mode it is possible to realign the LED cycles with the `s` command. In the batch mode, this will be applied automatically.

Anytime a command is received by the microcontroller, the `PB5` pin (D13 in Arduino terms) will be high for an short period of time and indicate this by flashing the onoard LED of the Arduino Nano. Additionally this will happen after startup, accompanied with an `I'm awake :)\x00` string via USART.

## Installation and Setup

This project requires `pyserial`, `avr-gcc` and `avrdude` for setup and usage and is tested with Python3 version 3.8. Install via

    sudo apt install python3-serial gcc-avr avrdude

Additionally you have to make sure to be in the `dialout` group to be permitted for using the serial port. The user `user_name` can be added by

    sudo adduser user_name dialout

### Firmware Flashing

The C program is written for an Arduino Nano with an ATmega328P. The device will be expected at the default `/dev/ttyUSB0` with LEDs attached to `PD[2:4]` (D2, D3, D4 in Arduino terms).

To build the firmware and flashing it to the Arduino Nano the following commands can be used. They have to be customized if used for a differing microcontroller (than ATmega328p) or platform and bootloader (than Arduino). After `cd path_to_directory/LedBlinker` type

    avr-gcc -std=c99 -mmcu=atmega328p -Os LedBlinker.c -o LedBlinker.elf;
    avr-objcopy -O ihex -j .text -j .data LedBlinker.elf LedBlinker.hex;
    avrdude -p atmega328p -c arduino -P /dev/ttyUSB0 -b 57600 -D -U flash:w:LedBlinker.hex:i;
    rm LedBlinker.elf LedBlinker.hex

After succsessful flashing type e.g.

    python3 LedBlinkerController.py

to access the controller script.

### Circuit Diagram

    PD4══150 Ohm══LED2══════╗
                            ║
    PD3══150 Ohm════LED1════╣
                            ║
    PD2══150 Ohm══════LED0══╣
                            ║
    GND═════════════════════╝

Using yellow LEDs with 2.2 V and 5 V power supply from the microcontroller we have approximately (5V - 2.2V) / 150 Ohm = 20 mA current on each pin.

![Image of the LEDs connected to an Arduino Nano](LedBlinker.jpeg)

#### Packet Format

The communication between the Python3 script and the microcontroller is based on packets. Each packet consists of 2 bytes per LED (e.g. if there are three LEDs the packet size is 6 byte). The packet starts with the most significant byte of the LED0 followed by the least significant byte of LED0, and so on.

Regarding the two bytes corresponding each LED there are four cases:
 - `0x0000` will not change the behavior (frequency or *off*) of the LED
 - `0x0001` will switch the LED off
 - any other value will apply as value for the half period length in ms (e.g. `0x01F4` for 2 Hz)
 - if every byte of the command is zero (`0x0000..0`) no LED will change its frequency or *off* state but the blinking LEDs will be aligned in terms of synchonicity.
